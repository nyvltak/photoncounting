#! /usr/bin/python

import numpy as np
import instrument
import time
import pylab
from play_sound import *
from timer import Timer


 

class Scope():
    
    def __init__(self, verbose = None):
        self.verbose = verbose
        self.photons_total = 0

        try:
            self.scope = instrument.RigolScope("/dev/usbtmc0")
        except:
            try:
                self.scope = instrument.RigolScope("/dev/usbtmc1")
            except:
                print('########################################')
                print('')
                print('Osciloskop se nepodarilo pripojit')
                print('Zkuste spustit program jako SUDO!')
                print('Pripadne zkontrolujte USB kabel!')
                print('')
                print('Pro spusteni bez sudo:')
                print('1/ Vytvorte soubor:')
                print('/etc/udev/rules.d/40-usbtmc-permissions.rules')# 
                print('2/ Napiste do nej (pripadne upravte s pomoci dat z lsusb):')
                print('ATTRS{idVendor}=="1ab1", ATTRS{idProduct}=="04b0", MODE="0660", GROUP="usbtmc"')
                print('3/ Vytvorte skupinu usbtmc')
                print('4/ Pridejte do ni uzivatele:')
                print('usermod -a -G usbtmc martin')
                print('5/ reboot')
                print('########################################')

        self.scope.write(":WAV:POIN:MODE NOR")
        self.scope.write(":WAV:FORM ASCii")
        self.pause = 0.1
    
    def ReadScope(self, channel):
        #self.scope.write(":STOP")
        self.scope.write(":WAV:SOUR CHAN"+ str(channel))
        time.sleep(self.pause)
        self.scope.write(":WAV:DATA?")
        rawdata = self.scope.read(20000)
        data=np.fromstring(rawdata,sep=',')
        #self.scope.write(":RUN")
        return data
    
    def ReadScopeIII(self):
        self.scope.write(":STOP")
        time.sleep(self.pause)
        self.scope.write(":WAV:SOUR CHAN1")
        time.sleep(self.pause)
        self.scope.write(":WAV:DATA?")
        rawdata1 = self.scope.read(20000)
        self.scope.write(":WAV:SOUR CHAN2")
        time.sleep(self.pause)
        self.scope.write(":WAV:DATA?")
        rawdata2 = self.scope.read(20000)
        data1=np.fromstring(rawdata1,sep=',')
        data2=np.fromstring(rawdata2,sep=',')
        self.scope.write(":RUN")
        time.sleep(self.pause)
        return data1,data2
   
  
    def MakeStatistics(self, channel, path, prefix, minwidth, maxwidth, threshold, N, show_graph = False, casova_zakladna = 100.0):
        photons = np.zeros(N)
        self.photons_total = 0
        for measurement in range(N):

            	#try:
                with Timer() as t:
                    d = self.ReadScope(channel)

                    # plot data
                    if show_graph:
                        plotData(d, threshold = threshold, minwidth = minwidth, maxwidth= maxwidth, casova_zakladna = casova_zakladna)
                                    
                    print path
		    np.save(path + '/' + prefix + '_meas_' + str(measurement+1) + '_data.npy',d) 
                    photon = CountPhotons(d,threshold,minwidth,maxwidth)
                    photons[measurement] = photon
                    
                    self.photons_total = self.photons_total + photon 
                    
            	#finally:
                print('{0} fotonu v mereni {1}. Celkem {2} fotonu. Jedno mereni {3} sec.'.format(str(photon), str(measurement+1), str(self.photons_total), t.interval))
            
                                                
        return photons
    
    def MakeStatisticsIII(self,path,prefix,minwidth,maxwidth,threshold,N,overlap,show_graph=False,casova_zakladna = 100.0):
        photons = np.zeros(N)
        for measurement in range(N):
            #print('measurement = ' + str(measurement+1))
            d = self.ReadScopeIII()
            np.save(path + '/' + prefix + '_meas_' + str(measurement+1) + '_data.npy',d) 
            photon1 = CountPhotons(d[0],threshold,minwidth,maxwidth)
            # plot data
            if show_graph:
                plotData(d, threshold = threshold, minwidth = minwidth, maxwidth= maxwidth, casova_zakladna = casova_zakladna)

            photon2 = CountPhotons(d[1],threshold,minwidth,maxwidth)
            photon_corelation = CorrelatePhotons(d,threshold,minwidth,maxwidth,overlap)
            photons[measurement] = photon_corelation
            print('Detekovano {0} a {1} fotonu v mereni {2}. Pocet korelaci {3}.'.format(str(photon1), str(photon2), str(measurement+1), str(photon_corelation)))
                
        return photons
    
   


############################################################
# FUNKCE
############################################################
def CountPhotons(data,detection_threshold,min_width,max_width):
    size = len(data)
    peakcount = 0
    width = 0
    width_increment = 0
    for position in range(1,size-1):
        if (data[position] < detection_threshold) & (data[position-1] >= detection_threshold):
            width_increment = 1
        if (data[position] > detection_threshold) & (data[position-1] <= detection_threshold):
            if (width > min_width) & (width < max_width):
                peakcount = peakcount + 1
                #play_detected()
                width_increment = 0
                width = 0
            else:
                width_increment = 0
                width = 0
                
        width = width + width_increment
        
    return peakcount
    
def PeaksPositions(data,detection_threshold,min_width,max_width):
    size = len(data)
    peaks = np.zeros(size)
    peakcount = 0
    width = 0
    width_increment = 0
    peak_start = 0
    for position in range(1,size-1):
        if (data[position] < detection_threshold) & (data[position-1] >= detection_threshold):
            width_increment = 1
            peak_start = position
        if (data[position] > detection_threshold) & (data[position-1] <= detection_threshold):
            if (width > min_width) & (width < max_width):
                peakcount = peakcount + 1
                width_increment = 0
                width = 0
                peaks[peak_start:position+1] = 1
            else:
                width_increment = 0
                width = 0
                
        width = width + width_increment
        
    return peaks

def PeaksPositions2(data,detection_threshold,min_width,max_width):
    size = len(data)
    peaks = np.zeros(size)
    peakcount = 0
    width = 0
    width_increment = 0
    peak_start = 0
    for position in range(1,size-1):
        if (data[position] < detection_threshold) & (data[position-1] >= detection_threshold):
            width_increment = 1
            peak_start = position
        if (data[position] > detection_threshold) & (data[position-1] <= detection_threshold):
            if (width > min_width) & (width < max_width):
                peakcount = peakcount + 1
                width_increment = 0
                width = 0
                peaks[peak_start] = 1
            else:
                width_increment = 0
                width = 0
                
        width = width + width_increment
        
    return peaks        

def CorrelatePhotons(data,detection_threshold,min_width,max_width,overlap):
    p1 = PeaksPositions(data[0],detection_threshold,min_width,max_width)
    p2 = PeaksPositions(data[1],detection_threshold,min_width,max_width)
    p = p1*p2
    
    size = len(p)
    peakcount = 0
    width = 0
    width_increment = 0
    for position in range(1,size-1):
        if (p[position] > 0.5) & (p[position-1] <= 0.5):
            width_increment = 1
        if (p[position] < 0.5) & (p[position-1] >= 0.5):
            if width > overlap:
                peakcount = peakcount + 1
                width_increment = 0
                width = 0
            else:
                width_increment = 0
                width = 0
                
        width = width + width_increment
        
    return peakcount

def plotData(data, threshold = None, minwidth = None, maxwidth= None, casova_zakladna = 100.0):

    pocet_fotonu = ''
    pylab.ion()
    pylab.clf()  
    
    if type(data) is tuple:
	pocet_grafu = len(data)
    else:
        pocet_grafu = 1
   
    for graf in range(pocet_grafu):
    	# vykresli hranici threshold
        
        if type(data) is tuple:
	    d = data[graf]
        else:
            d = data
	xx = np.array(range(d.shape[0]), np.float) /100 * casova_zakladna
        pylab.subplot(pocet_grafu, 1, graf)
    	if threshold is not None:
            thresh_plot = np.asarray(d.copy())
            thresh_plot[:] = threshold
            pylab.plot(xx,thresh_plot)     
            pylab.text(0, threshold, str(threshold)) 

        # vykresli pozice detekovanych fotonu, vypis pocty fotonu
        if threshold is not None and minwidth is not None and maxwidth is not None:
            pozice = PeaksPositions(d,threshold,minwidth,maxwidth)
            x = 0
            for i in pozice:
                x += 1
                if i == 1.0:
                    pylab.plot(xx[x], 0, 'rs') 
            # vypis pocty detekovanych fotonu
            pocet_fotonu = CountPhotons(d,threshold,minwidth,maxwidth)
        
            p1 = pylab.plot(xx, d, label=str(pocet_fotonu))
            ax = pylab.gca()
            legend = ax.legend(loc='upper right', shadow=True)
        else:
        
            p1 = pylab.plot(xx, d)
              
        pylab.ylim([-0.020,0.01])          
    pylab.draw()          
        
def MakeHistogram(self,photons):
    hist = np.histogram(photons,range(40))
    np.save('meas_photons.npy',photons)
    np.save('meas_histogram.npy',hist)
    print(hist)
    return hist       
      
if __name__ == '__main__':
    S = Scope()
    #S.SetScopeSettings(1)
    p = S.MakeStatistics(10000)
    S.MakeHistogram(p)
    
    
