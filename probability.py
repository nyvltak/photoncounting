from __future__ import division
import pylab
import numpy as np


def factorial_r(n):
    if n < 2:
        return 1
    return n * factorial_r(n-1)

def Poisson(n, average_no_photons, scale_factor = 1, sampling_points = 1400):
    tau = np.asarray(range(1400))

    a = average_no_photons
    T = sampling_points * scale_factor
    P_n = np.power((a/T * tau), n)    /(factorial_r(n)) * np.exp(-a/T*tau)
    
    return P_n

def Bose(n, average_no_photons, scale_factor = 1, sampling_points = 1400):
    tau = np.asarray(range(1400))

    a = average_no_photons
    T = sampling_points * scale_factor
    P_n = np.power((a/T * tau), n) / np.power((1 + (a/T * tau)), n + 1) #(factorial_r(n)) * np.exp(-a/T*tau)
    
    return P_n    

def plotPoisson():
    average_no_photons = 6.74
    no_of_measurements = 10000

    style_poisson = 'r-.'
    style_bose = 'b:'

    pylab.subplot(3, 1, 1)
    p0 = Poisson(0,average_no_photons)
    b0 = Bose(0,average_no_photons)
    pylab.plot(no_of_measurements * p0, style_poisson, label='Poisson')
    pylab.plot(no_of_measurements * b0, style_bose, label='Bose-Einstein')
    ax = pylab.gca()
    legend = ax.legend(loc='upper right', shadow=True)

    
    pylab.ylabel('0 fotonu')

    pylab.subplot(3, 1, 2)
    p1 = Poisson(1, average_no_photons)
    b1 = Bose(1, average_no_photons)
    pylab.plot(no_of_measurements * p1)
    pylab.plot(no_of_measurements * b1)
    pylab.ylabel('1 foton')

    pylab.subplot(3, 1, 3)
    p2 = Poisson(2, average_no_photons)
    b2 = Bose(2, average_no_photons)
    pylab.plot(no_of_measurements * p2)
    pylab.plot(no_of_measurements * b2)
    pylab.ylabel('2 fotony')



    pylab.show()

if __name__ == '__main__':
    plotPoisson()
