#!/usr/bin/env python

import numpy as np
import pylab
import instrument
from lib_rigol import *
from nonblocking_input import *
from play_sound import *
import sys
import os
import re
import time
from probability import * 

path = 'data'
os.system('clear')

try:
    s = Scope();
    print 'Osciloskop pripojen'
    print ''
except:
    print 'Osciloskop neni dostupny! Lze pouze zpracovat jiz namerena data!'
    print ''

if not os.path.isdir(path):
    print('Adresar "{}" neexistuje, bude vytvoren.'.format(path))
    os.mkdir(path)
    
prefix = raw_input('Zadejte prefix pro jmena souboru: ')
casova_zakladna = float(raw_input('Zadejte delku casove zakladny (1 dilek) [ns]: '))

while True:
    
    os.system('clear')
    print 'Prefix: ', prefix
    action = raw_input('''Vyberte typ akce:   1 - statisticke mereni, 
                    2 - prohlizeni a zpracovani vysledku, 
                    3 - testovaci mereni, 
                    4 - kontinualni testovaci mereni, 
                    5 - zmena prefixu, 
                    6 - preulozit data do csv,
                    q - ukonceni programu\n ''')
    os.system('clear')
    
    if action == '1':
        
        detectors = raw_input('Vyberte pocet detektoru [1/2]: ')
        pulse_width_min = int(raw_input('Zadejte minimalni delku detekovaneho pulzu [pt](0): '))
        pulse_width_max = int(raw_input('Zadejte maximalni delku detekovaneho pulzu [pt]:(6) '))
        pulse_threshold = float(raw_input('Zadejte prahovou hodnotu pro detekci pulzu [V](-0.004): '))
        N = int(raw_input('Zadejte pocet mereni pro vytvoreni statistiky: '))
        show_graphs = raw_input('Zobrazovat grafy v prubehu mereni [a/n]: ')
        delay_start = raw_input('Opozdeny start. [s]: ')
        
        if re.search('[ayAY1]', show_graphs) is not None:
            show_graphs = True
        else:
            show_graphs = False
        
        if detectors == '1':
            detector = raw_input('Zvolte mereny kanal [1/2]: ')
            raw_input('Nastavte rezim osciloskopu pro mereni a zmacknete ENTER ...')
            if delay_start is not '':                
                print 'Cekam ', delay_start, ' sekund.'
                time.sleep(int(delay_start))   
            os.system('clear')

            print('Probiha mereni')
            if detector == '1':
                photons = s.MakeStatistics(detector,path,prefix,pulse_width_min,pulse_width_max,pulse_threshold,N, show_graphs, casova_zakladna = casova_zakladna)
                np.save(path + '/' + prefix + '_photons.npy',photons)                
            elif detector == '2':
                photons = s.MakeStatistics(detector,path,prefix,pulse_width_min,pulse_width_max,pulse_threshold,N, show_graphs, casova_zakladna = casova_zakladna)
                np.save(path + '/' + prefix + '_photons.npy',photons)
            else:
                raw_input('Neplatna volba! Pokracujte zmacknutim ENTER!')
            play_sound()
            raw_input('Mereni uspesne dokonceno! Zmacknete ENTER!')
                
        elif detectors == '2':
            overlap = int(raw_input('Zadejte pozadovany prekryv pulsu pri korelaci [pt]: '))
            if delay_start is not '':                
                print 'Cekam ', delay_start, ' sekund.'
                time.sleep(int(delay_start)) 
            photons = s.MakeStatisticsIII(path,prefix,pulse_width_min,pulse_width_max,pulse_threshold,N,overlap, show_graph=show_graphs, casova_zakladna = casova_zakladna)
            np.save(path + '/' + prefix + '_photons.npy',photons)

        else:
            raw_input('Neplatna volba! Pokracujte zmacknutim ENTER!')
        
    elif action == '2':
        subaction = raw_input('''Vyberte operaci 
            1-prohlizeni konkretniho mereni, 
            2-histogram, 
            3-casovy vyvoj poctu fotonu
            4-vyvoj poctu detekci 0,1,2 fotonu

             ''')
        cont = True
        if subaction == '1':
            while cont == True:
                os.system('clear')
                measurement = raw_input('Zadejte cislo mereni: ')
                try:
                    a = np.load(path + '/' + prefix + '_meas_' + measurement + '_data.npy')
                    photons = np.load(path + '/' + prefix + '_photons.npy')
                    print photons[int(measurement)-1]
                    if a.shape[0] == 2:
                        pylab.plot(a[0])
                        pylab.plot(a[1])
                        pylab.show()
                    else:
                        pylab.plot(a)
                        pylab.show()
                except:
                    print('Hledany soubor nelze otevrit!')
                
                conts = raw_input('Dalsi mereni? [a/n]: ')
                if conts == 'a':
                    pass
                else:
                    cont = False
                    
        elif subaction == '2':
            try:
                photons = np.load(path + '/' + prefix + '_photons.npy')
                bins = int(raw_input('Zadejte pocet kontejneru histogramu: '))
                hist = np.histogram(photons,range(bins))
                pylab.plot(hist[0])
                pylab.show()
            except:
                print('Hledany soubor nelze otevrit!')
        elif subaction == '3':
            try:
                pylab.ion()
                pylab.clf()
                photons = np.load(path + '/' + prefix + '_photons.npy')
                average_photons = np.average(photons)
                print 'Average number of photons', average_photons

                plt_average_photons = np.asarray(photons.copy())
                plt_average_photons[:] = average_photons
                pylab.plot(photons)
                pylab.plot(plt_average_photons, 'r')
                pylab.xlabel('Cislo mereni')
                pylab.ylabel('Pocet fotonu')
                pylab.title('Prumerny pocet poctu fotonu = "{}" .'.format(str(average_photons)))
                pylab.show()
            except:
                print('Hledany soubor nelze otevrit!')     
        elif subaction == '4':
            
            photons = np.load(path + '/' + prefix + '_photons.npy')
            average_photons = np.average(photons)
            print 'Average number of photons', average_photons

            no_of_measurements = photons.shape[0]
            vyvoj_fotonu_v_case = np.zeros([1400,6000])

            for meas_nr in range(no_of_measurements):
                a = np.load(path + '/' + prefix + '_meas_' + str(meas_nr + 1) + '_data.npy')
                
                pozice = PeaksPositions2(a, detection_threshold = -0.004, min_width = 0,max_width = 6)  
                x = 0
                pos0 = 0
                foton = 0
                for i in pozice:
                    x += 1
                    if i == 1.0:
                        vyvoj_fotonu_v_case[pos0:x, foton] += 1
                        pos0 = x
                        foton += 1          

            style_meas = 'b'
            style_poisson = 'g-.'
            style_bose = 'r:'

            pylab.ion()
            pylab.clf()

            casova_zakladna

            scale_factor = 100.0/casova_zakladna

            pylab.subplot(3, 1, 1)  
            pylab.plot(vyvoj_fotonu_v_case[:, 0], style_meas, label='Mereni')
            pylab.plot(no_of_measurements * Poisson(0,average_photons,  scale_factor = scale_factor), style_poisson, label='Poisson')
            pylab.plot(no_of_measurements * Bose(0,average_photons,  scale_factor = scale_factor), style_bose, label='Bose-Einstein')
            pylab.ylabel('0 fotonu')
            ax = pylab.gca()
            legend = ax.legend(loc='upper right', shadow=True)

            pylab.subplot(3, 1, 2)
            pylab.plot(vyvoj_fotonu_v_case[:, 1], style_meas, label='Mereni')
            pylab.plot(no_of_measurements * Poisson(1,average_photons,  scale_factor = scale_factor), style_poisson, label='Poisson')
            pylab.plot(no_of_measurements * Bose(1,average_photons,  scale_factor = scale_factor), style_bose, label='Bose-Einstein')
            pylab.ylabel('1 foton')

            pylab.subplot(3, 1, 3)
            pylab.plot(vyvoj_fotonu_v_case[:, 2], style_meas, label='Mereni')
            pylab.plot(no_of_measurements * Poisson(2,average_photons,  scale_factor = scale_factor), style_poisson, label='Poisson')
            pylab.plot(no_of_measurements * Bose(2,average_photons,  scale_factor = scale_factor), style_bose, label='Bose-Einstein')
            pylab.ylabel('2 fotony')

            pylab.draw()          

        else:
            raw_input('Neplatna volba! Pokracujte zmacknutim ENTER!')
            
    elif action == '3':
        channel = raw_input('Zvolte kanal pro testovaci mereni [1/2]: ')
        if channel == '1':
            data = s.ReadScope(1)
            pylab.plot(data)
            pylab.show()
        elif channel == '2':
            data = s.ReadScope(2)
            pylab.plot(data)
            pylab.show()
        else:
            raw_input('Neplatna volba! Pokracujte zmacknutim ENTER!')

    elif action == '4':
        channel = raw_input('Zvolte kanal pro testovaci mereni [1/2]: ')
        
        print('Pro ukonceni stisknete ESC.')
        with NonBlockingConsole() as nbc:
            i = 0
            while 1:
                if channel.find('1') is not -1:
                    data = s.ReadScope(1)
                    plotData(data, casova_zakladna = casova_zakladna)                    
                elif channel.find('2') is not -1:
                    data = s.ReadScope(2)
                    plotData(data, casova_zakladna = casova_zakladna)  
                else:
                    raw_input('Neplatna volba kanalu! Pokracujte zmacknutim ENTER!')

                if nbc.get_data() == '\x1b':  # x1b is ESC
                    pylab.close()
                    break  
                pylab.ioff() 
        
    elif action == '5':
        prefix = raw_input('Zadejte prefix pro jmena souboru: ')
        
    elif action == '6':
        try:
            photons = np.load(path + '/' + prefix + '_photons.npy')
            np.savetxt(path + '/' + prefix + '_photons.csv',photons,delimiter=',')
        except:
            raw_input('Cannot convert file ' + path + '/' + prefix + '_photons.npy ! Press ENTER ...')
        filenumber = 1
        file = True
        while file == True:
            try:
                data = np.load(path + '/' + prefix + '_meas_' + str(filenumber) + '_data.npy')
                np.savetxt(path + '/' + prefix + '_meas_' + str(filenumber) + '_data.csv',data,delimiter=',')
                filenumber = filenumber + 1
            except:
                file = False    
    elif action == 'q':
        '''
        Exit application
        '''
        sys.exit()                
        
    else:
        raw_input('Neplatna volba! Pokracujte zmacknutim ENTER!')
    
    
